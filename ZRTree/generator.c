#include <stdio.h>
#include <stdlib.h>
int main(int argc, char **argv){
  if(argc !=2){
    printf("ERRO| DIGITE O NUMERO DE RECTS\n");
    return 1;
  }
  FILE* fp;
  fp = fopen("generated","w+");

  int i, MAX = atoi(*(argv+1));
  fprintf(fp,"%d\n",MAX);
  for(i=0;i<MAX;i++)
    fprintf(fp,"%d\n%d\n%d\n%d\n",i,i,i+1,i+1);
  fclose(fp);
  return 0;
}
