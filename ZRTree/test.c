
#include "index.h"
#include <stdio.h>
#include <math.h>

struct Rect rects[] = {
  {{0,0,1,1}},
  {{1,1,2,2}},
  {{2,2,3,3}},
};
int nrects = sizeof(rects) / sizeof(rects[0]);

struct Rect search_rect3 = {
  {0, 0, 10000, 10000}, // search will find above rects that this one overlaps
};

int MySearchCallback(int id, void* arg)
{
  // Note: -1 to make up for the +1 when data was inserted
  /* printf("%d>> Hit data rect %d\n",pthread_self(), id-1); */
  return 1; // keep going
}

int main(int argc, char **argv){

  if(argc!=2){
    printf("ERROR DIGITE O NOME DO ARQUIVO GERADOR\n");
    exit(1);
  }
  FILE * pFile;
  char mystring[100];
  pFile = fopen(*(argv+1),"r");
  if (pFile == NULL) perror ("Error opening file");
  else{
    int a = 0,testsize = 0;

    if( fgets (mystring , 100 , pFile) != NULL )
    testsize = strtol(mystring,NULL,10);

    struct Rect rtest[testsize];
    int testint = sizeof(rtest) / sizeof(rtest[0]);
    int cont = 0;
    while( fgets (mystring , 100 , pFile) != NULL ){
      if(mystring[0]!='\n'){
        a= strtol(mystring,NULL,10);
        printf("CONT: %d|TESTSIZE: %d|NUMSIDES: %d\n",cont,testsize,NUMSIDES );
        printf("VALUE: %d|IN THE %d RTEST INDEX(cont/NUMSIDES)|IN THE %d BOUNDARY(cont%NUMSIDES)\n\n",a,(cont/NUMSIDES),cont%NUMSIDES );

        rtest[cont/NUMSIDES].boundary[cont%NUMSIDES] = a;
        cont++;
        //printf("%d\n",a);
      }
    }





    struct Node* root = RTreeNewIndex();
    int i, nhits,nhits2,nhits3;
    clock_t start, end;
    double nothr, timethr;
    threads = 0;
    total_threads = 0;

    //void* nhits3;
    printf("nrects = %d\n", nrects);
    /*
    * Insert all the data rects.
    * Notes about the arguments:
    * parameter 1 is the rect being inserted,
    * parameter 2 is its ID. NOTE: *** ID MUST NEVER BE ZERO ***, hence the +1,
    * parameter 3 is the root of the tree. Note: its address is passed
    * because it can change as a result of this call, therefore no other parts
    * of this code should stash its address since it could change undernieth.
    * parameter 4 is always zero which means to add from the root.
    */

    //for(i=0; i<nrects; i++)
    //	RTreeInsertRect(&rects[i], i+1, &root, 0); // i+1 is rect ID. Note: root can change
    RTreePrintRect(&rtest[0],0);
    RTreePrintRect(&rtest[1],0);
    RTreePrintRect(&rtest[2],0);
    for(i=0; i<testsize; i++)
    RTreeInsertRect(&rtest[i], i+1, &root, 0); // i+1 is rect ID. Note: root can change
    struct Search temp;
    temp.N=root;
    temp.R=&search_rect3;
    temp.shcb=&MySearchCallback;
    temp.cbarg=NULL;
    int resposta =0;
    temp.hits = &resposta;

    /* printf("\n\n\n"); */
    /* RTreePrintNode(root,0); */

    start = clock();
    nhits3 = RTreeSearch(root, &search_rect3, MySearchCallback, 0);
    end = clock();

    nothr = (double)(end - start)/CLOCKS_PER_SEC;
    pthread_t thread_main;

    start = clock();
    pthread_create(&thread_main,NULL,RTreeSearch3,&temp);
    //nhits3 = RTreeSearch3(&temp);
    void* returns;
    int teste_main;
    teste_main = pthread_join(thread_main,NULL);
    end = clock();
    if(teste_main!=0){
      printf("ERRO NO MAIN PTHREAD_JOIN\n");
    }
    else{
      printf("SEM ERRO NO MAIN PTHREAD_JOIN\n" );
    }

    printf("Total de threads criadas: %d\n", total_threads);
    timethr = (double)(end - start)/CLOCKS_PER_SEC;
    printf("RTREESEARCH: %d | RTREESEARCH3: %d\nRTREESEARCH: %.6lf | RTREESEARCH3: %.6lf\n",nhits3,resposta, nothr, timethr);

    printf("TEST FILA\n");
    struct Queue* q;
    //q = malloc(sizeof(struct Queue));
    QueueInit(&q);
    QueuePush(&q,root);
    struct Node* n = QueuePop(&q);
    if(n==NULL)
    printf("TA NULL\n");
    RTreePrintNode(n,0);
    if(QueuePop(&q)==NULL)
    printf("EH ISSO\n" );

    QueueKill(&q);
    printf("END\n");
    fclose (pFile);
  }
  return 0;
}
